// Get the necessary elements
const taskInput = document.getElementById("taskInput");
const addTaskBtn = document.getElementById("addTaskBtn");
const taskList = document.getElementById("taskList");

// Add event listener to the "Add Task" button
addTaskBtn.addEventListener("click", addTask);

// Function to add a new task
function addTask() {
  const taskText = taskInput.value.trim();

  if (taskText !== "") {
    const taskItem = document.createElement("li");
    taskItem.classList.add("flex", "items-center");

    const taskCheckbox = document.createElement("input");
    taskCheckbox.type = "checkbox";
    taskCheckbox.classList.add("mr-2");
    taskCheckbox.addEventListener("change", toggleTask);

    const taskTextElement = document.createElement("span");
    taskTextElement.textContent = taskText;
    taskTextElement.classList.add("flex-grow");

    const deleteBtn = document.createElement("button");
    deleteBtn.innerHTML = '<i class="fas fa-trash"></i>';
    deleteBtn.classList.add(
      "ml-2",
      "px-2",
      "py-1",
      "bg-black",
      "text-white",
      "rounded",
      "hover:bg-gray-700",
      "focus:outline-none",
      "focus:bg-gray-700",
    );

    deleteBtn.addEventListener("click", deleteTask);

    taskItem.appendChild(taskCheckbox);
    taskItem.appendChild(taskTextElement);
    taskItem.appendChild(deleteBtn);

    taskList.appendChild(taskItem);

    taskInput.value = "";
  }
}

// Function to toggle the task's completion status
function toggleTask(event) {
  const taskItem = event.target.parentNode;
  taskItem.classList.toggle("line-through");
}

// Function to delete a task with animation
function deleteTask(event) {
  const taskItem = event.target.parentNode;
  taskItem.classList.add("fade-out");

  setTimeout(() => {
    taskList.removeChild(taskItem);
  }, 500); // delay of 0.5s to match the animation duration
}
